package operationservice.interfacemodule.services

import org.springframework.stereotype.Service

import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.Exchange
import org.springframework.amqp.rabbit.annotation.Queue
import org.springframework.amqp.rabbit.annotation.QueueBinding
import org.springframework.amqp.rabbit.annotation.RabbitListener

import org.slf4j.LoggerFactory

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.MismatchedInputException

import operationservice.interfacemodule.contracts.UserContract


@Service
class UpdateUserResourcesConsumer(
        private val resourcePublisher: UpdatedResourcesPublisher
) {
    private val logger = LoggerFactory.getLogger(UpdateUserResourcesConsumer::class.java)

    @RabbitListener(bindings = [QueueBinding(
            value = Queue(value = "operation-service.update-resources", durable = "false"),
            exchange = Exchange(
                    value = "update-resources",
                    durable = "false",
                    ignoreDeclarationExceptions = "true"
            )
    )])
    fun processToUpdateUserResources(message: Message) {
        val decodedJson = String(message.body)

        logger.info("[x] message received: {}", decodedJson)

        if (decodedJson.isEmpty()) {
            logger.info("message skip by empty content: {}", decodedJson)

            return
        }

        try {
            ObjectMapper().readValue(decodedJson, UserContract::class.java)
                    .toEntity()
                    .fold(
                            { user -> resourcePublisher.processToPublishResources(user) },
                            { error -> println(error) }
                    )
        } catch (exception: MismatchedInputException) {
            logger.info(
                    "can't deserialize message content to UserContract: {}, {}",
                    decodedJson,
                    exception.message
            )
        } catch (exception: Exception) {
            logger.info(
                    "something wrong: {}, {}",
                    decodedJson,
                    exception.message
            )
        }
    }
}