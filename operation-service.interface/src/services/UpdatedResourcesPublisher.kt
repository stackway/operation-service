package operationservice.interfacemodule.services

import org.springframework.stereotype.Component
import org.springframework.amqp.core.AmqpTemplate

import org.slf4j.LoggerFactory

import operationservice.application.services.ResourceArrangeService
import operationservice.domain.entities.User

@Component
class UpdatedResourcesPublisher(
        private val amqpTemplate: AmqpTemplate,
        private val resourcesArrangeService: ResourceArrangeService
) {
    private val logger = LoggerFactory.getLogger(UpdatedResourcesPublisher::class.java)

    fun processToPublishResources(user: User) {
        val resources = resourcesArrangeService.arrange(user.id)

        if (resources.isEmpty()) {
            logger.info(
                    "skip message publish by empty resources: {}",
                    user.email
            )

           return
        }

        resources.forEach {
            amqpTemplate.convertAndSend(
                    "user-resources",
                    "",
                    it
            )

            logger.info("[-] message published: {}", user.email)
        }
    }
}