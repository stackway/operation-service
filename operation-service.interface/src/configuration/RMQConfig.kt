package operationservice.interfacemodule.configuration

import org.springframework.amqp.core.*
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Bean

import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory
import org.springframework.amqp.rabbit.connection.Connection
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter


@Configuration
@EnableRabbit
class RMQConfig(
        private val rmqProps: RMQProps
) {
    @Bean
    fun connectionFactory(): ConnectionFactory {
        val connection = CachingConnectionFactory(rmqProps.host)

        connection.username = rmqProps.user
        connection.setPassword(rmqProps.password)

        return connection
    }

    @Bean
    fun connection(): Connection {
        return connectionFactory().createConnection()
    }

    @Bean
    fun amqpAdmin(): AmqpAdmin {
        val amqpAdmin = RabbitAdmin(connectionFactory())

        val userResources = DirectExchange(
                "user-resources",
                false,
                false
        )
        amqpAdmin.declareExchange(userResources)

        return amqpAdmin
    }

    @Bean
    fun rabbitTemplate(): RabbitTemplate {
        val template = RabbitTemplate(connectionFactory())

        template.messageConverter = messageConverter()

        return template
    }

    @Bean
    fun messageConverter(): Jackson2JsonMessageConverter {
        return Jackson2JsonMessageConverter()
    }
}