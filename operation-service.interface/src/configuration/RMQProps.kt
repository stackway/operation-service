package operationservice.interfacemodule.configuration

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component

@Component
class RMQProps(private val environment: Environment) {
    val host: String = environment.getProperty("RMQ_HOST", "localhost")
    val user: String = environment.getProperty("RMQ_USER", "guest")
    val password: String = environment.getProperty("RMQ_PASSWORD", "guest")
}