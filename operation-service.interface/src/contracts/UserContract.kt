package operationservice.interfacemodule.contracts

import com.fasterxml.jackson.annotation.JsonAlias
import java.lang.IllegalArgumentException
import java.io.Serializable

import com.fasterxml.jackson.annotation.JsonProperty

import com.github.kittinunf.result.Result

import operationservice.domain.entities.User

class UserContract(
        @JsonAlias("userId", "UserId") @JsonProperty("userId")
        val userId: Long? = null,
        @JsonAlias("userEmail", "UserEmail") @JsonProperty("userEmail")
        val userEmail: String? = null
) {
    fun toEntity(): Result<User, IllegalArgumentException> {
        if (userId == null || userEmail == null) {
            return Result.of {
                throw IllegalArgumentException(
                        "userId: $userId, userEmail: $userEmail"
                )
            }
        }

        return Result.of { User(userId, userEmail) }
    }
}