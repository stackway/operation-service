package operationservice.interfacemodule

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["operationservice"])
class OperationServiceApplication

fun main(args: Array<String>) {
	runApplication<OperationServiceApplication>(*args)
}
