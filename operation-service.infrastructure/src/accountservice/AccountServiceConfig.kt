package operationservice.infrastructure.accountservice

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component

@Component
class AccountServiceConfig(environment: Environment) {
    val host: String = environment.getProperty(
            "ACCOUNT_SERVICE_HOST",
            "http://localhost"
    )

    val port: String = environment.getProperty(
            "ACCOUNT_SERVICE_PORT",
            "8080"
    )
}