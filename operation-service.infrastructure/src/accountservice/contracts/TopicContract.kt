package operationservice.infrastructure.accountservice.contracts

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty

import org.slf4j.LoggerFactory

import operationservice.domain.entities.Topic
import operationservice.domain.entities.TopicType

class TopicContract(
        @JsonAlias("id", "Id")
        @JsonProperty("Id")
        var id: Long? = 0,

        @JsonAlias("name", "Title")
        @JsonProperty("Title")
        var name: String? = null,

        @JsonAlias("type", "Type")
        @JsonProperty("Type")
        var type: TopicType? = TopicType.BY_USED_TAGS
) {
    private val logger = LoggerFactory.getLogger(TopicContract::class.java)

    fun toEntity(): Topic? {
        if (id == null) {
            logger.info("can't construct entity: id is null")

            return null
        }

        if (type == null) {
            logger.info("can't construct entity: type is null")

            return null
        }

        return Topic(id!!, type!!)
    }
}