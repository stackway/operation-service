package operationservice.infrastructure.accountservice.contracts

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty

import java.time.LocalDateTime

import org.slf4j.LoggerFactory

import operationservice.domain.entities.Tag
import java.util.*

class TagContract(
        @JsonAlias("id", "Id")
        @JsonProperty("Id")
        var id: Long? = 0,

        @JsonAlias("name", "Title")
        @JsonProperty("Title")
        var name: String? = "",

        @JsonAlias("views", "Views")
        @JsonProperty("Views")
        var views: Int? = 0,

        @JsonAlias("recentlyUsed", "RecentlyUsed")
        @JsonProperty("RecentlyUsed")
        var recentlyUsed: Date,

        @JsonAlias("importance", "Importance")
        @JsonProperty("Importance")
        var importance: Int = 1
) {
    private val logger = LoggerFactory.getLogger(TagContract::class.java)

    fun toEntity(): Tag? {
        if (id == null) {
            logger.info("can't construct entity: id is null")

            return null
        }

        if (name == null) {
            logger.info("can't construct entity: name is null")

            return null
        }

        if (views == null) {
            logger.info("can't construct entity: views is null")

            return null
        }

        return Tag(id!!, name!!, views!!, recentlyUsed, importance)
    }
}