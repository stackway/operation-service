package operationservice.infrastructure.accountservice

import retrofit2.Call

import operationservice.infrastructure.accountservice.contracts.TagContract
import operationservice.infrastructure.accountservice.contracts.TopicContract
import operationservice.infrastructure.searchservice.contracts.ResourceContract
import retrofit2.http.*

interface AccountServiceClient {
    @GET("internal/api/users/{userId}/tags/")
    fun getAllTags(@Path("userId") userId: Long): Call<List<TagContract>>

    @GET("internal/api/users/{userId}/tags/recently/")
    fun getRecentlyUsedTags(@Path("userId") userId: Long): Call<List<TagContract>>

    @GET("internal/api/users/{userId}/tags/mostly-used/")
    fun getMostlyUsedTags(@Path("userId") userId: Long): Call<List<TagContract>>

    @GET("internal/api/users/{userId}/topics/")
    fun getAllTopics(@Path("userId") userId: Long): Call<List<TopicContract>>

    @POST("internal/api/users/{userId}/resources/filter-unique")
    fun getUnreadedResources(
            @Path("userId") userId: Long,
            @Body resourcesUrls: List<String>
    ): Call<List<String>>

    @GET("internal/api/users/{userId}/communities/resources")
    fun getResourcesFromSimilarUsers(
            @Path("userId") userId: Long,
            @Query("takeFromUser") takeFromUser: Int
    ): Call<List<ResourceContract>>
}