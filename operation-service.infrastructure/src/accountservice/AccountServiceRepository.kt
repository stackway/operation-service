package operationservice.infrastructure.accountservice

import java.io.IOException

import org.springframework.stereotype.Service

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.Retrofit

import operationservice.domain.entities.Tag
import operationservice.domain.entities.Topic
import operationservice.infrastructure.searchservice.contracts.ResourceContract

@Service
class AccountServiceRepository(config: AccountServiceConfig) {
    private lateinit var client: AccountServiceClient

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("${config.host}:${config.port}/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build()

        client = retrofit.create(AccountServiceClient::class.java)
    }

    fun getAllTags(userId: Long): List<Tag> {
        val response = client.getAllTags(userId).execute()

        if (!response.isSuccessful) {
            processException(response.errorBody())
        }

        val contracts = processBody(response).map { it.toEntity() }
        return contracts.filterNotNull()
    }

    fun getRecentlyUsedTags(userId: Long): List<Tag> {
        val response = client.getRecentlyUsedTags(userId).execute()

        if (!response.isSuccessful) {
            processException(response.errorBody())
        }

        val contracts = processBody(response).map { it.toEntity() }
        return contracts.filterNotNull()
    }

    fun getMostlyUsedTags(userId: Long): List<Tag> {
        val response = client.getMostlyUsedTags(userId).execute()

        if (!response.isSuccessful) {
            processException(response.errorBody())
        }

        val contracts = processBody(response).map { it.toEntity() }
        return contracts.filterNotNull()
    }

    fun getAllTopics(userId: Long): List<Topic> {
        val response = client.getAllTopics(userId).execute()

        if (!response.isSuccessful) {
            processException(response.errorBody())
        }

        val contracts = processBody(response).map { it.toEntity() }
        return contracts.filterNotNull()
    }

    fun getUnreadedResources(
            userId: Long,
            resourcesUrls: List<String>
    ): List<String> {
        val response = client
                .getUnreadedResources(userId, resourcesUrls)
                .execute()

        if (!response.isSuccessful) {
            processException(response.errorBody())
        }

        return processBody(response)
    }

    fun getResourcesFromSimilarUsers(
            userId: Long,
            takeFromUser: Int
    ): List<ResourceContract> {
        val response = client
                .getResourcesFromSimilarUsers(userId, takeFromUser)
                .execute()

        if (!response.isSuccessful) {
            processException(response.errorBody())
        }

        return processBody(response)
    }

    private fun processException(error: ResponseBody?) {
        throw IOException(
                error?.string() ?: "Unknown error"
        )
    }

    private fun <T> processBody(response: Response<T>): T {
        return response.body() ?: throw IOException("body is empty")
    }
}