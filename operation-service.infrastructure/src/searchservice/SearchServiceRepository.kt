package operationservice.infrastructure.searchservice

import java.io.IOException

import org.springframework.stereotype.Service

import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.Response

import operationservice.infrastructure.searchservice.contracts.ResourceQuery
import operationservice.infrastructure.searchservice.contracts.ResourceContract

@Service
class SearchServiceRepository(config: SearchServiceConfig) {
    private lateinit var client: SearchServiceClient

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("${config.host}:${config.port}/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build()

        client = retrofit.create(SearchServiceClient::class.java)
    }

    fun getResources(query: ResourceQuery): List<ResourceContract> {
        val response = client.get(query).execute()

        if (!response.isSuccessful) {
            processException(response.errorBody())
        }

        return processBody(response)
    }

    private fun processException(error: ResponseBody?) {
        throw IOException(
                error?.string() ?: "Unknown error"
        )
    }

    private fun <T> processBody(response: Response<T>): T {
        return response.body() ?: throw IOException("body is empty")
    }
}