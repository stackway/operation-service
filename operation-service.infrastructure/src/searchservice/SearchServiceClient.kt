package operationservice.infrastructure.searchservice

import retrofit2.Call
import retrofit2.http.Body

import operationservice.infrastructure.searchservice.contracts.ResourceContract
import operationservice.infrastructure.searchservice.contracts.ResourceQuery
import retrofit2.http.POST

interface SearchServiceClient {
    @POST("internal/api/resources/common/")
    fun get(@Body query: ResourceQuery): Call<List<ResourceContract>>
}