package operationservice.infrastructure.searchservice.contracts

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty

class ResourceQuery(
        @JsonAlias("baseTerms", "BaseTerms")
        @JsonProperty("BaseTerms")
        val baseTerms: String,

        @JsonAlias("optionalTerms", "OptionalTerms")
        @JsonProperty("OptionalTerms")
        val optionalTerms: String = "",

        @JsonAlias("startAt", "StartAt")
        @JsonProperty("StartAt")
        val startAt: Int = scaledRandom(1, 2),

        @JsonAlias("number", "Number")
        @JsonProperty("Number")
        val number: Int = scaledRandom(5, 10),

        @JsonAlias("dateRestrict", "DateRestrict")
        @JsonProperty("DateRestrict")
        val dateRestrict: String = "m1",

        @JsonAlias("queryTags", "QueryTags")
        @JsonProperty("QueryTags")
        val queryTags: List<String> = listOf()
)

fun scaledRandom(from: Int, to: Int): Int {
    return (Math.random() * (to - from) + from).toInt()
}