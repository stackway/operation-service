package operationservice.infrastructure.searchservice.contracts

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty

import java.util.*

class ResourceContract(
        @JsonAlias("title", "Title")
        @JsonProperty("Title")
        var Title: String,

        @JsonAlias("description", "Description")
        @JsonProperty("Description")
        var Description: String,

        @JsonAlias("publishedAt", "PublishedAt")
        @JsonProperty("PublishedAt")
        var PublishedAt: Date? = null,

        @JsonAlias("displayLink", "DisplayLink")
        @JsonProperty("DisplayLink")
        var DisplayLink: String,

        @JsonAlias("link", "Link")
        @JsonProperty("Link")
        var Link: String,

        @JsonAlias("imageLink", "ImageLink")
        @JsonProperty("ImageLink")
        var ImageLink: String? = null,

        @JsonAlias("relevanceIndex", "RelevanceIndex")
        @JsonProperty("RelevanceIndex")
        var RelevanceIndex: Double,

        @JsonAlias("tags", "Tags")
        @JsonProperty("Tags")
        var Tags: List<String> = listOf(),

        @JsonAlias("queryTags", "QueryTags")
        @JsonProperty("QueryTags")
        var QueryTags: List<String> = listOf()
)