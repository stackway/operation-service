package operationservice.infrastructure.searchservice

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component

@Component
class SearchServiceConfig(environment: Environment) {
    val host: String = environment.getProperty(
            "SEARCH_SERVICE_HOST",
            "http://localhost"
    )

    val port: String = environment.getProperty(
            "SEARCH_SERVICE_PORT",
            "8080"
    )
}