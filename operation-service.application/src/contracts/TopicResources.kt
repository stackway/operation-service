package operationservice.application.contracts

import operationservice.domain.entities.Topic
import operationservice.infrastructure.searchservice.contracts.ResourceContract

class TopicResources(
        val topic: Topic,
        val resources: List<ResourceContract>
)