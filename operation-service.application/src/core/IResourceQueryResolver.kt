package operationservice.application.core

import operationservice.infrastructure.searchservice.contracts.ResourceQuery

interface IResourceQueryResolver {
    fun resolve(): ResourceQuery
}