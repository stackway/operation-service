package operationservice.application.core

import org.springframework.stereotype.Component

import operationservice.domain.entities.Tag
import operationservice.infrastructure.searchservice.contracts.ResourceQuery

@Component("BaseQueryResolver")
class ResourceQueryResolver(val chain: List<Tag>) : IResourceQueryResolver {
    override fun resolve(): ResourceQuery {
        if (chain.isEmpty()) {
            return ResourceQuery("")
        }

        val tagNames = chain
                .sortedBy { it.importance }
                .map { it.name }

        if (tagNames.size == 1) {
            return ResourceQuery(
                    tagNames.first(),
                    queryTags = chain.map { it.name },
                    dateRestrict = "m2"
            )
        }

        if (tagNames.last().split(' ').size > 2) {
            return ResourceQuery(
                    tagNames.last(),
                    queryTags = listOf(tagNames.last()),
                    dateRestrict = "m2"
            )
        }

        return ResourceQuery(
                tagNames.last(),
                tagNames.dropLast(1).joinToString(" "),
                queryTags = chain.map { it.name },
                dateRestrict = "m2"
        )
    }
}