package operationservice.application.core

import operationservice.domain.entities.Tag
import operationservice.infrastructure.searchservice.contracts.ResourceQuery
import org.springframework.stereotype.Component

@Component("LongTermQueryResolver")
class LongTermResourceQueryResolver(val chain: List<Tag>) : IResourceQueryResolver {
    override fun resolve(): ResourceQuery {
        if (chain.isEmpty())
            return ResourceQuery("")

        val tagNames = chain.map { it.name }

        return ResourceQuery(
                "${tagNames.first()} ${tagNames.random()}",
                tagNames.joinToString(" "),
                queryTags = chain.map { it.name },
                dateRestrict = "m6"
        )
    }
}