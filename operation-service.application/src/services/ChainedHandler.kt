package operationservice.application.services

import operationservice.domain.entities.Tag
import operationservice.domain.entities.Topic

open class ChainedHandler<T>(
        private val handler: ProcessableTopic<T>,
        vararg handlers: ProcessableTopic<T>
) : ProcessableTopic<T> {
    private var pipeline: ProcessableTopic<T>? = null

    init {
        if (handlers.isNotEmpty()) {
            pipeline = ChainedHandler<T>(
                    handlers.first(),
                    *handlers.drop(1).toTypedArray()
            )
        }
    }

    override fun handle(userId: Long, topics: List<Topic>, tags: List<Tag>): List<T> {
        var foundResources = handler.handle(userId, topics, tags)

        if (pipeline != null) {
           foundResources = foundResources.plus(pipeline!!.handle(userId, topics, tags))
        }

        return foundResources
    }
}
