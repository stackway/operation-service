package operationservice.application.services

import org.springframework.stereotype.Component

import operationservice.application.contracts.TopicResources
import operationservice.application.core.ResourceQueryResolver
import operationservice.domain.actions.ChainedTags
import operationservice.domain.entities.Tag
import operationservice.domain.entities.Topic
import operationservice.domain.entities.TopicType
import operationservice.infrastructure.accountservice.AccountServiceRepository
import operationservice.infrastructure.searchservice.SearchServiceRepository
import operationservice.infrastructure.searchservice.contracts.ResourceContract
import org.slf4j.LoggerFactory

@Component
class ResourcesByUsedTagsHandler(
        private val resourcesRepository: SearchServiceRepository,
        private val accountRepository: AccountServiceRepository
) : ProcessableTopic<TopicResources> {
    private val logger = LoggerFactory.getLogger(ResourcesByUsedTagsHandler::class.java)

    override fun handle(userId: Long, topics: List<Topic>, tags: List<Tag>): List<TopicResources> {
        if (!topics.any { it.type == TopicType.BY_USED_TAGS }) {
            return listOf()
        }

        if (tags.isEmpty()) {
            logger.warn(
                    "can't find topic: {}",
                    TopicType.BY_USED_TAGS
            )

            return listOf()
        }

        val topic = topics.first {
            it.type == TopicType.BY_USED_TAGS
        }

        var foundResources = mutableListOf<ResourceContract>()

        for (i in 0..2) {
            val query = ResourceQueryResolver(
                    ChainedTags().byViewsEntries(
                            tags,
                            // move to the options
                            2
                    )
            )

            foundResources.addAll(
                    resourcesRepository.getResources(query.resolve())
            )
        }

        foundResources = foundResources
                .distinctBy { it.Link }
                .toMutableList()

        val unreadResourcesLinks = accountRepository.getUnreadedResources(
                userId,
                foundResources.map { it.Link }
        )

        foundResources = foundResources
                .filter { unreadResourcesLinks.contains(it.Link) }
                .toMutableList()

        return listOf(TopicResources(topic, foundResources))
    }
}