package operationservice.application.services

import org.springframework.stereotype.Component

import operationservice.application.contracts.TopicResources

@Component
class ProcessableTopicPipeline(
        resourcesByUsedTagsHandler: ResourcesByUsedTagsHandler,
        resourcesByRandomTagsHandler: ResourcesByRandomTagsHandler,
        resourcesByRecentlyViewedTagsHandler: ResourcesByRecentlyViewedTagsHandler,
        resourcesBySimilarUsersHandler: ResourcesBySimilarUsersHandler
) : ChainedHandler<TopicResources>(
        resourcesByUsedTagsHandler,
        resourcesByRandomTagsHandler,
        resourcesByRecentlyViewedTagsHandler,
        resourcesBySimilarUsersHandler
)