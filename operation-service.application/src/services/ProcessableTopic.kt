package operationservice.application.services

import operationservice.domain.entities.Tag
import operationservice.domain.entities.Topic

interface ProcessableTopic<T> {
    fun handle(userId: Long, topics: List<Topic>, tags: List<Tag>): List<T>
}