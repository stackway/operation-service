package operationservice.application.services

import java.io.IOException

import org.slf4j.LoggerFactory

import org.springframework.stereotype.Service

import operationservice.application.contracts.TopicResources
import operationservice.infrastructure.accountservice.AccountServiceRepository

@Service
class ResourceArrangeService(
        private val accountRepository: AccountServiceRepository,
        private val processingPipeline: ProcessableTopicPipeline
) {
    private val logger = LoggerFactory.getLogger(ResourceArrangeService::class.java)

    fun arrange(userId: Long): List<TopicResources> {
        try {
            val topics = accountRepository.getAllTopics(userId)

            if (topics.isEmpty()) {
                logger.info("user: {} has't topics", userId)
            }

            logger.info("topic in use: {}", topics.joinToString(" ") { it.type.value })

            val tags = accountRepository.getAllTags(userId)

            if (tags.isEmpty()) {
                logger.info("user: {} has't tags", userId)
            }

            return processingPipeline.handle(
                    userId,
                    topics,
                    tags
            )
        } catch (exception: IOException) {
            var message = exception.message

            if (message.isNullOrEmpty()) {
                logger.info(
                        "something wrong: {}",
                        exception
                )
            } else {
                logger.info(
                        "something wrong: {}",
                        message
                )
            }

            return listOf()
        }
    }
}