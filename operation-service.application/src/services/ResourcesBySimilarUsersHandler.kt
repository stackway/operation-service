package operationservice.application.services

import org.springframework.stereotype.Component

import org.slf4j.LoggerFactory

import operationservice.domain.entities.Tag
import operationservice.domain.entities.Topic
import operationservice.infrastructure.accountservice.AccountServiceRepository
import operationservice.application.contracts.TopicResources
import operationservice.domain.entities.TopicType

@Component
class ResourcesBySimilarUsersHandler(
        private val accountRepository: AccountServiceRepository
) : ProcessableTopic<TopicResources> {
    private val logger = LoggerFactory.getLogger(ResourcesBySimilarUsersHandler::class.java)

    override fun handle(userId: Long, topics: List<Topic>, tags: List<Tag>): List<TopicResources> {
        if (!topics.any { it.type == TopicType.BY_SIMILAR_USERS }) {
            return listOf()
        }

        val topic = topics.first {
            it.type == TopicType.BY_USED_TAGS
        }

        var foundResources = accountRepository.getResourcesFromSimilarUsers(userId, 5)

        if (foundResources.isEmpty()) {
            logger.warn(
                    "user doesn't have any resources from similar users: {}",
                    userId
            )

            return listOf()
        }

        foundResources = foundResources
                .distinctBy { it.Link }
                .toMutableList()

        val unreadResourcesLinks = accountRepository.getUnreadedResources(
                userId,
                foundResources.map { it.Link }
        )

        foundResources = foundResources
                .filter { unreadResourcesLinks.contains(it.Link) }
                .toMutableList()

        return listOf(TopicResources(topic, foundResources))
    }
}