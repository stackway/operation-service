package operationservice.domain.entities

class User(
        val id: Long,
        val email: String
)