package operationservice.domain.entities

enum class TopicType(val value: String) {
    BY_USED_TAGS("by-used-tags"),
    BY_RECENTLY_VIEWED_TAGS("by-recently-viewed-tags"),
    BY_SIMILAR_USERS("by-similar-users"),
    BY_RANDOM_TAGS("by-random-tags")
}