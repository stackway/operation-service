package operationservice.domain.entities

class Topic(
        var id: Long = 0,
        var type: TopicType = TopicType.BY_USED_TAGS
)