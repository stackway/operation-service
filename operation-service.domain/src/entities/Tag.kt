package operationservice.domain.entities

import java.util.*

class Tag(
        var id: Long = 0,
        var name: String = "",
        var views: Int = 0,
        var recentlyUsed: Date,
        var importance: Int = 1
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }

        if (javaClass != other?.javaClass) {
            return false
        }

        other as Tag
        if (name != other.name) {
            return false
        }

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}