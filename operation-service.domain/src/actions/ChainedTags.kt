package operationservice.domain.actions

import operationservice.domain.entities.Tag

class ChainedTags {
    fun byRandomEntries(tags: List<Tag>, maxChain: Int): List<Tag> {
        val chain = mutableListOf<Tag>()

        if (tags.isEmpty()) {
            return chain
        }

        for (i in 0..maxChain) {
            val tagToAdd = tags.random()

            if (tagToAdd in chain) {
                continue
            }

            chain.add(tagToAdd)
        }

        return chain
    }

    fun byViewsEntries(tags: List<Tag>, maxChain: Int): List<Tag> {
        val chain = mutableListOf<Tag>()

        if (tags.isEmpty()) {
            return chain
        }

        val sortedTagsByViews = tags.sortedBy { it.views }
                .take(
                        Math.pow(
                                maxChain.toDouble(),
                                2.0
                        ).toInt()
                )

        for (i in 0..maxChain) {
            val tagToAdd = sortedTagsByViews.random()

            if (tagToAdd in chain) {
                continue
            }

            chain.add(tagToAdd)
        }

        return chain
    }

    fun byRecentlyUsedEntries(tags: List<Tag>, maxChain: Int): List<Tag> {
        val chain = mutableListOf<Tag>()

        if (tags.isEmpty()) {
            return chain
        }

        val sortedTagsByUsed = tags.sortedBy { it.recentlyUsed }
                .take(
                        Math.pow(
                                maxChain.toDouble(),
                                2.0
                        ).toInt()
                )

        for (i in 0..maxChain) {
            val tagToAdd = sortedTagsByUsed.random()

            if (tagToAdd in chain) {
                continue
            }

            chain.add(tagToAdd)
        }

        return chain
    }
}