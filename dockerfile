# Stage 1
FROM openjdk:8 AS builder
WORKDIR /source

# first copy gradle and cache them
COPY ./gradlew ./
COPY ./gradle ./gradle
RUN ./gradlew

# and after copy the build files for cache all dependencies
COPY build.gradle settings.gradle ./

COPY *.domain/build.gradle ./operation-service.domain/build.gradle
COPY *.infrastructure/build.gradle ./operation-service.infrastructure/build.gradle
COPY *.application/build.gradle ./operation-service.application/build.gradle
COPY *.interface/build.gradle ./operation-service.interface/build.gradle

RUN ./gradlew classes

COPY *.domain ./operation-service.domain
COPY *.infrastructure ./operation-service.infrastructure
COPY *.application ./operation-service.application
COPY *.interface ./operation-service.interface

# at the end run gradlew bootJar
RUN ./gradlew operation-service.interface:bootJar
# COPY ./operation-service.interface/build/libs/operation-service.interface-0.1.0.jar /app/operation-service-interface.jar

COPY ./scripts/ /app/scripts/

COPY ./docker-entrypoint.sh /app/docker-entrypoint.sh
RUN mv ./operation-service.interface/build/libs/operation-service.interface-0.1.0.jar /app/operation-service-interface.jar

# Stage 2
FROM openjdk:8
WORKDIR /app
COPY --from=builder /app .
ENTRYPOINT ["./docker-entrypoint.sh"]
