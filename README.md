# Overview

## Resources

### Spring AMQP

- <https://docs.spring.io/spring-amqp/docs/2.1.4.RELEASE/reference/#async-annotation-driven>
- <https://docs.spring.io/spring-amqp/docs/2.1.4.RELEASE/reference/#sending-messages>

### REST

- <https://square.github.io/retrofit/>
- <https://auth0.com/blog/developing-a-restful-client-with-retrofit-and-spring-boot/>
